---
redirect_from:
 - /aboutus
title: About Us
permalink: /about
---

Our group started work in February 2004, achieved rough consensus on version 0 in April 2004, did implementations and fine tuning throughout summer and fall 2004, and declared the tuned version to be version 1 in January 2005. During the spring of 2005 we settled into a permanent home at [Xiph.org](http://xiph.org). Between August and November of 2006 we unfroze the version 1 document for a final round of improvements to the drafting and formatting, limiting changes to those which did not require any implementation to be modified. In fall of 2021 we redesigned and re-engineered the web site.

Ian C. Rogers and Robert Kaye bootstrapped our project. Dave Brown, Dan Brickley, and Kevin Marks contributed ideas which had a strong influence on architecture and syntax. Sebastian Pipping and Ivo Emanuel Gonçalves were the drivers behind the 2006 revisions. Sebastian Pipping created the online validator. Evan Boehs and Tess Gadwa led the 2021 work with assistance from Ralph Giles. Lucas Gonze, Bjorn Wijers and Sebastian Pipping created the first round of web resources like the applications and extensions directories. Lucas Gonze, Matthias Friedrich, and Robert Kaye authored the specification documents. 

We are grateful for comments and feedback from Ryan Shaw, Alf Eaton, Steve Gedikian, Russell Garrett, Ben Tesch and Pau Garcia i Quiles. Special thanks to the developers Tomas Franzén (who participated in our work from the very beginning), Jim Garrison, Brander Lien, Fabricio Zuardi and to everyone who contributed their time and skill on the mailing list and wiki.

We are grateful to the Metabrainz foundation for their generosity in founding the project and to Xiph.org Foundation for supporting us ever since. We are in awe of Fabricio Zuardi, who popularized the format by creating [a player](http://musicplayer.sourceforge.net) which became a runaway hit.
