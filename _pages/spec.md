---
permalink: spec
redirect_from:
- specs
title: Spec
---

Abstract
========

We describe an XML playlist format that is open, moderately simple, and carefully engineered.

* * *

Table of Contents
=================

1 [Introduction](#1-introduction)  
1.1 [Example](#11-example)  
2 [Administration](2-administration)  
2.1 [History](#21-history)  
2.2 [Acknowledgements](#22-acknowledgements)  
2.3 [terminology](23-terminology)  
2.3.1 [URI, URLs and URNs](#231-uri-urls-and-urns)  
2.3.2 [Requirements notation](#232-requirements-notation)  
3 [Abstractions](#3-abstractions)  
3.1 [Defining playlists](#31-defining-playlists)  
3.2 [What a playlist is not](#32-what-a-playlist-is-not)  
3.3 [Shareability](#33-shareability)  
3.4 [Content resolver](#34-content-resolver)  
3.5 [Fuzzy names](#35-fuzzy-names)  
4 [Element definitions](#4-element-definitions)  
4.1 [elements](#41-elements)  
4.1.1 [playlist](#411-playlist)  
4.1.1.1 [attributes](#4111-attributes)  
4.1.1.1.1 [xmlns](#41111-xmlns)  
4.1.1.1.2 [version](#41112-version)  
4.1.1.2 [elements](#4112-elements)  
4.1.1.2.1 [title](#41121-title)  
4.1.1.2.2 [creator](#41122-creator)  
4.1.1.2.3 [annotation](#41123-annotation)  
4.1.1.2.4 [info](#41124-info)  
4.1.1.2.5 [location](#41125-location)  
4.1.1.2.6 [identifier](#41126-identifier)  
4.1.1.2.7 [image](#41127-image)  
4.1.1.2.8 [date](#41128-date)  
4.1.1.2.9 [license](#41129-license)  
4.1.1.2.10 [attribution](#411210-attribution)  
4.1.1.2.11 [link](#411211-link)  
4.1.1.2.11.1 [attributes](#4112111-attributes)  
4.1.1.2.11.1.1 [rel](#41121111-rel)  
4.1.1.2.11.2 [content](#4112112-content)  
4.1.1.2.12 [meta](#411212-meta)  
4.1.1.2.12.1 [attributes](#4112121-attributes)  
4.1.1.2.12.1.1 [rel](#41121211-rel)  
4.1.1.2.12.2 [content](#4112122-content)  
4.1.1.2.13 [extension](#411212-extension)  
4.1.1.2.13.1 [attributes](#4112131-attributes)  
4.1.1.2.13.1.1 [application](#41121311-application)  
4.1.1.2.13.2 [content](#4112132-content)  
4.1.1.2.14 [trackList](#411214-tracklist)  
4.1.1.2.14.1 [elements](#4112141-elements)  
4.1.1.2.14.1.1 [track](#41121411-track)  
4.1.1.2.14.1.1.1 [elements](#411214111-elements)  
4.1.1.2.14.1.1.1.1 [location](#4112141111-location)  
4.1.1.2.14.1.1.1.2 [identifier](#4112141112-identifier)  
4.1.1.2.14.1.1.1.3 [title](#4112141113-title)  
4.1.1.2.14.1.1.1.4 [creator](#4112141114-creator)  
4.1.1.2.14.1.1.1.5 [annotation](#4112141115-annotation)  
4.1.1.2.14.1.1.1.6 [info](#4112141116-info)  
4.1.1.2.14.1.1.1.7 [image](#4112141117-image)  
4.1.1.2.14.1.1.1.8 [album](#4112141118-album)  
4.1.1.2.14.1.1.1.9 [trackNum](#4112141119-trackNum)  
4.1.1.2.14.1.1.1.10 [duration](#41121411110-duration)  
4.1.1.2.14.1.1.1.11 [link](#41121411111-link)  
4.1.1.2.14.1.1.1.11.1 [attributes](#411214111111-attributes)  
4.1.1.2.14.1.1.1.11.1.1 [rel](#4112141111111-rel)  
4.1.1.2.14.1.1.1.11.2 [content](#41121411112-content)  
4.1.1.2.14.1.1.1.12 [meta](#41121411112-meta)  
4.1.1.2.14.1.1.1.12.1 [attributes](#411214111121-attributes)  
4.1.1.2.14.1.1.1.12.1.1 [rel](#4112141111211-rel)  
4.1.1.2.14.1.1.1.12.2 [content](#411214111122-content)  
4.1.1.2.14.1.1.1.13 [extension](#41121411113-extension)  
4.1.1.2.14.1.1.1.13.1 [attributes](#411214111131-attributes)  
4.1.1.2.14.1.1.1.13.1.1 [application](#4112141111311-application)  
4.1.1.2.14.1.1.1.13.2 [content](#411214111132-content)  
5 [Requirements for XSPF generators](#5-requirements-for-xspf-generators)  
6 [Requirements for XSPF players](#6-requirements-for-xspf-players)  
6.1 [Graceful failure](#61-graceful-failure)  
6.2 [Relative paths](#62-relative-paths)  
6.3 [Extension URIs](#63-extension-uris)  
§ [References](#references)  
§ [Author's Addresses](#authors-addresses)  
§ [Copyright and Patent Statements](#copyright-and-patent-statements)  

* * *

1 Introduction
==============

There is no XML format for playlists that can measure up to the standards of the formats for web pages (HTML), weblogs (RSS/Atom), and web graphs (RDF/XML). It is evident that there is a need, because XML is the preferred data description language of the moment and as a result the tools and skills to use it are ubiquitous.

It is also evident that existing playlist formats fall short. ASX (for Windows Media Player) and the iTunes library format are proprietary. ASX resembles XML in that it uses angle brackets, but is not XML by any means. M3U, RAM, and M4U are flat files; QuickTime is binary; Pls is in the Windows .ini format; Gnomoradio RDF is RDF, not XML. SMIL addresses a much larger problem space than the average digital music player. The timing model of RSS is incompatible with playlists. Few of these formats are well documented. None of these formats make simple features easy to code and hard features possible. Only one is an open standard. Not one offers playlist interoperability across major vendors.

The question for software developers is _why should I support this new XML playlist format_? The choice is between M3U and SMIL. Almost every digital music player accepts M3U but also invents an XML playlist format. Inventing a format creates work, for example to study related formats; you should use XSPF to avoid the work. SMIL, on the other hand, is a prescription for a kind of application that is different from a typical music player — it describes layouts in time, while XSPF describes concepts common among music players. Given a song with the comment "danceable!", SMIL might have an instruction to write that text in the upper left in a bold sans-serif font, while XSPF would tell a player that the text is a comment and say nothing about formatting.

1.1 Example
-----------

A very simple document looks like this:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<playlist version="1" xmlns="http://xspf.org/ns/0/">
  <trackList>
    <track><location>file:///music/song\_1.ogg</location></track>
    <track><location>file:///music/song\_2.flac</location></track>
    <track><location>file:///music/song\_3.mp3</location></track>
  </trackList>
</playlist>
```

or this:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<playlist version="1" xmlns="http://xspf.org/ns/0/">
  <trackList>
     <track><location>http://example.net/song\_1.ogg</location></track>
     <track><location>http://example.net/song\_2.flac</location></track>
     <track><location>http://example.com/song\_3.mp3</location></track>
   </trackList>
 </playlist>
```

* * *

2 Administration
================

2.1 History
-----------

Our group started work in February 2004, achieved rough consensus on version 0 in April 2004, did implementations and fine tuning throughout summer and fall 2004, and declared the tuned version to be version 1 in January 2005. Between August and November of 2006 we unfroze this document for a final round of improvements to the drafting, limiting changes to those which did not require any implementation to be modified.

2.2 Acknowledgements
--------------------

Ian C. Rogers and Robert Kaye bootstrapped our project. Dave Brown, Dan Brickley, and Kevin Marks contributed ideas which had a strong influence on architecture and syntax. Sebastian Pipping and Ivo Emanuel Gonçalves were the drivers behind the 2006 revisions.

We are grateful for comments and feedback from Ryan Shaw, Alf Eaton, Steve Gedikian, Russell Garrett, Ben Tesch and Pau Garcia i Quiles. Special thanks to the developers Tomas Franzén (who participated in our work from the very beginning), Jim Garrison, Brander Lien, and Fabricio Zuardi, and to everyone who contributed their time and skill on the mailing list and wiki.

We would especially like to thank our hosts at the the Xiph.org foundation and Metabrainz foundation for their continuing generosity.

2.3 terminology
---------------

### 2.3.1 URI, URLs and URNs

We use the term URI in this document regardless of whether a given URI specifies the location of a resource (a URL) or its name (a URN) independent of location.

See also: [RFC2396bis](http://gbiv.com/protocols/uri/rev-2002/rfc2396bis.html); [URIs, URLs, and URNs: Clarifications and Recommendations 1.0](http://www.w3.org/TR/uri-clarification/)

### 2.3.2 Requirements notation

The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL NOT", "SHOULD", "SHOULD NOT", "RECOMMENDED", "MAY", and "OPTIONAL" in this document are to be interpreted as described in [\[RFC2119\]](#RFC2119 "Key words for use in
RFCs to Indicate Requirement Levels").

* * *

3 Abstractions
==============

3.1 Defining playlists
----------------------

An XSPF playlist describes a sequence of objects to be rendered. Objects might be audio, video, text, playlists, or any other media type. The function of a playlist is to identify the objects and communicate their order.

3.2 What a playlist is not
--------------------------

The function of a playlist _is not_ to communicate metadata about the composer, song title, etc. Metadata is hard and there are many providers already. We decided that we couldn't compete, and that there was no need for us to try. Moreover, good metadata does not travel well — every user has to recreate it. Metadata should come from external sources and namespaces like MusicBrainz or Gracenote; this what the XSPF _link_ and _meta_ elements are for.

The function of a playlist _is not_ to store derived information about objects that a user has a copy of. A playlist is not a catalog. A catalog is computed across hard data like files; it stores information like filesystem paths and the contents of ID3 tags. This data has no value on any machine but the one on which it originated. Sharing this data would be a privacy and security violation. Software which needs access to this data has no reason to maintain it in a standard format, because it has no reason to allow access to it. Standardizing this data would be fruitless, because there are an endless number of measurements that software might take and store. Derived information belongs in a _catalog_.

Things a playlist is not, then, are a metadata format or a catalog. We took care to enable these features, but also to avoid duplicating their functionality, poorly.

3.3 Shareability
----------------

If there is no reason for a playlist to be shared, there is no need for a new format. Even a buggy format does no damage if it is created and consumed by the same software on the same machine. The need for a new format only comes up when a playlist travels from one machine to another, for example when it is published on the internet.

One type of shareability is between different pieces of software on the same machine. It is common for playlists created with one application to not be usable by another application on the same machine, because of different or conflicting interpretations of the playlist format. M3U suffers from this very badly, because M3U playlists often reference files according to a base path which changes from application to application. The XSPF group aimed to fix this by providing unambiguous definitions.

The other type of shareability is between different machines. For playlists to be meaningful on different machines, they must be able to identify network resources. Audio and video objects are often abstractions like "movie X by director Y" rather than computer-friendly objects like "whatever file can be gotten from the URI http://foo/x/y". To handle this problem, we have provided support for media objects to be found via queries; XSPF identifiers are _fuzzy names_.

3.4 Content resolver
--------------------

On a surface level you can use XSPF like any other playlist format. Drop a bunch of filenames into an XSPF document, prepend "file://" to each, and you're ready to go. Under the surface there is much more.

The guiding design principle was to separate the functionality of a catalog of files from the functionality of a list of songs. Most software music players on the PC have some sort of cache for file information. This cache stores a list, or catalog, of available files and metadata from ID3 tags and other sources. XSPF is not a catalog format. XSPF exists only to say which songs to play. Almost everything in XSPF is for the purpose of answering the question _which resource_, rather than the question _what is this resource_.

If XSPF is not a catalog format, what is it? XSPF is an intermediate format. We expected a new kind of software called a _content resolver_ to do the job of converting XSPF to a plain old list of files or URIs. A content resolver would be smart enough to keep your playlists from breaking when you move your media from /oggs to /music/ogg. It would be able to figure out that a playlist entry by the artist "Hank Williams" with the title "Your Cheating Heart" could be satisfied by the file /vorbis/hankwilliams/yourcheatingheart.ogg. It might even know how to query the iTunes music store or another online provider to locate and download a missing song.

The content resolver maintains the catalog of your songs in whatever format it prefers. It might use a flatfile, a file in the Berkeley DB format, or a SQL database. It might use only ID3 metadata, but it might also know how to query MusicBrainz or another metadata service.

All XSPF user agents are content resolvers, in that they have complete leeway to turn the contents of a track element into a specific set of bytes.

3.5 Fuzzy names
---------------

Any given track can be identified in a number of ways. We provided means for absolute identifiers like URIs, filesystem paths and secure hashes, but also for query-based identifiers — free text fields like artist and work title and numeric fields for song length, all of which together should be enough for a good content resolver to turn into files.

* * *

4 Element definitions
=====================

4.1 elements
------------

### 4.1.1 playlist

### 4.1.1.1 attributes

### 4.1.1.1.1 xmlns

http://xspf.org/ns/0/

### 4.1.1.1.2 version

1

Notice that the namespace is 0 but the version is 1. This is because version 1 playlists are backwards compatible with version 0 parsers.

### 4.1.1.2 elements

### 4.1.1.2.1 title

A human-readable title for the playlist. xspf:playlist elements MAY contain exactly one.

### 4.1.1.2.2 creator

Human-readable name of the entity (author, authors, group, company, etc) that authored the playlist. xspf:playlist elements MAY contain exactly one.

### 4.1.1.2.3 annotation

A human-readable comment on the playlist. This is character data, not HTML, and it may not contain markup. xspf:playlist elements MAY contain exactly one.

### 4.1.1.2.4 info

URI of a web page to find out more about this playlist. Likely to be homepage of the author, and would be used to find out more about the author and to find more playlists by the author. xspf:playlist elements MAY contain exactly one.

### 4.1.1.2.5 location

Source URI for this playlist. xspf:playlist elements MAY contain exactly one.

### 4.1.1.2.6 identifier

Canonical ID for this playlist. Likely to be a hash or other location-independent name. MUST be a legal URI. xspf:playlist elements MAY contain exactly one.

### 4.1.1.2.7 image

URI of an image to display in the absence of a //playlist/trackList/image element. xspf:playlist elements MAY contain exactly one.

### 4.1.1.2.8 date

Creation date (not last-modified date) of the playlist, formatted as a [XML schema dateTime](http://www.w3.org/TR/xmlschema-2/#dateTime). xspf:playlist elements MAY contain exactly one.

A sample date is "2005-01-08T17:10:47-05:00". PHP to produce such a string from a unix timestamp is:

```php
$main\_date = date("Y-m-d\\TH:i:s", $timestamp);
$tz = date("O", $timestamp);  
$tz = substr\_replace ($tz, ':', 3, 0);
```

In the absence of a timezone, the element MAY be assumed to use Coordinated Universal Time (UTC, sometimes called "Greenwich Mean Time").

Note: in version 0 of XSPF, this was specifed as an ISO 8601 date. xsd:dateTime is the same thing (with better documentation) for almost every date in history, and there are no playlist creation dates that might be different.

### 4.1.1.2.9 license

URI of a resource that describes the license under which this playlist was released. xspf:playlist elements may contain zero or one license element.

### 4.1.1.2.10 attribution

An ordered list of URIs. The purpose is to satisfy licenses allowing modification but requiring attribution. If you modify such a playlist, move its //playlist/location or //playlist/identifier element to the top of the items in the //playlist/attribution element. xspf:playlist elements MAY contain exactly one xspf:attribution element.

Such a list can grow without limit, so as a practical matter we suggest deleting ancestors more than ten generations back.

```xml
<attribution>
  <location>http://bar.com/modified\_version\_of\_original\_playlist.xspf</location>
  <identifier>somescheme:original\_playlist.xspf</identifier>
</attribution>
```

### 4.1.1.2.11 link

The link element allows XSPF to be extended without the use of XML namespaces. xspf:playlist elements MAY contain zero or more link elements.

`<link rel="http://foaf.example.org/namespace/version1">http://socialnetwork.example.org/foaf/mary.rdfs</link>`

### 4.1.1.2.11.1 attributes

### 4.1.1.2.11.1.1 rel

URI of a resource type.

### 4.1.1.2.11.2 content

URI of a resource.

### 4.1.1.2.12 meta

The meta element allows metadata fields to be added to XSPF. xspf:playlist elements MAY contain zero or more meta elements.

`<meta rel="http://example.org/key">value</meta>`

### 4.1.1.2.12.1 attributes

### 4.1.1.2.12.1.1 rel

URI of a resource defining the metadata.

### 4.1.1.2.12.2 content

Value of the metadata element. This is plain old text, not XML, and it may not contain markup. xspf:playlist elements MAY contain exactly one.

### 4.1.1.2.13 extension

The extension element allows non-XSPF XML to be included in XSPF documents. The purpose is to allow nested XML, which the meta and link elements do not. xspf:playlist elements MAY contain zero or more extension elements.

```xml
<playlist version="1" xmlns="http://xspf.org/ns/0/" xmlns:cl="http://example.com">
  <extension application="http://example.com">
    <cl:clip start="25000" end="34500"/>
  </extension>
  <trackList />
</playlist>
```

### 4.1.1.2.13.1 attributes

### 4.1.1.2.13.1.1 application

URI of a resource defining the structure and purpose of the nested XML.

### 4.1.1.2.13.2 content

nested XML.

### 4.1.1.2.14 trackList

Ordered list of xspf:track elements to be rendered. The sequence is a hint, not a requirement; renderers are advised to play tracks from top to bottom unless there is an indication otherwise.

If an xspf:track element cannot be rendered, a user-agent MUST skip to the next xspf:track element and MUST NOT interrupt the sequence.

xspf:playlist elements MUST contain one and only one trackList element. The trackList element my be empty.

### 4.1.1.2.14.1 elements

### 4.1.1.2.14.1.1 track

### 4.1.1.2.14.1.1.1 elements

### 4.1.1.2.14.1.1.1.1 location

URI of resource to be rendered. Probably an audio resource, but MAY be any type of resource with a well-known duration, such as video, a SMIL document, or an XSPF document. The duration of the resource defined in this element defines the duration of rendering. xspf:track elements MAY contain zero or more location elements, but a user-agent MUST NOT render more than one of the named resources.

### 4.1.1.2.14.1.1.1.2 identifier

Canonical ID for this resource. Likely to be a hash or other location-independent name, such as a MusicBrainz identifier. MUST be a legal URI. xspf:track elements MAY contain zero or more identifier elements.

For example, the URI [http://musicbrainz.org/track/7e1d6f5f-0ac3-4889-8b57-506a67b459fc.html](http://musicbrainz.org/track/7e1d6f5f-0ac3-4889-8b57-506a67b459fc.html) is an identifier for a specific song, but dereferencing that identifier will not yield a copy of the song.

### 4.1.1.2.14.1.1.1.3 title

Human-readable name of the track that authored the resource which defines the duration of track rendering. This value is primarily for fuzzy lookups, though a user-agent may display it. xspf:track elements MAY contain exactly one.

### 4.1.1.2.14.1.1.1.4 creator

Human-readable name of the entity (author, authors, group, company, etc) that authored the resource which defines the duration of track rendering. This value is primarily for fuzzy lookups, though a user-agent may display it. xspf:track elements MAY contain exactly one.

### 4.1.1.2.14.1.1.1.5 annotation

A human-readable comment on the track. This is character data, not HTML, and it may not contain markup. xspf:track elements MAY contain exactly one.

### 4.1.1.2.14.1.1.1.6 info

URI of a place where this resource can be bought or more info can be found. xspf:track elements MAY contain exactly one.

### 4.1.1.2.14.1.1.1.7 image

URI of an image to display for the duration of the track. xspf:track elements MAY contain exactly one.

### 4.1.1.2.14.1.1.1.8 album

Human-readable name of the collection from which the resource which defines the duration of track rendering comes. For a song originally published as a part of a CD or LP, this would be the title of the original release. This value is primarily for fuzzy lookups, though a user-agent may display it. xspf:track elements MAY contain exactly one.

### 4.1.1.2.14.1.1.1.9 trackNum

Integer with value greater than zero giving the ordinal position of the media on the xspf:album. This value is primarily for fuzzy lookups, though a user-agent may display it. xspf:track elements MAY contain exactly one. It MUST be a valid [XML Schema nonNegativeInteger](http://www.w3.org/TR/xmlschema-2/#dt-nonNegativeInteger).

### 4.1.1.2.14.1.1.1.10 duration

The time to render a resource, in milliseconds. It MUST be a valid [XML Schema nonNegativeInteger](http://www.w3.org/TR/xmlschema-2/#dt-nonNegativeInteger). This value is only a hint — different XSPF generators will generate slightly different values. A user-agent MUST NOT use this value to determine the rendering duration, since the data will likely be low quality. xspf:track elements MAY contain exactly one duration element.

### 4.1.1.2.14.1.1.1.11 link

The link element allows XSPF to be extended without the use of XML namespaces. xspf:track elements MAY contain zero or more link elements.

`<link rel="http://foaf.org/namespace/version1">http://socialnetwork.org/foaf/mary.rdfs</link>`

### 4.1.1.2.14.1.1.1.11.1 attributes

### 4.1.1.2.14.1.1.1.11.1.1 rel

URI of a resource type.

### 4.1.1.2.14.1.1.1.11.2 content

URI of a resource.

### 4.1.1.2.14.1.1.1.12 meta

The meta element allows metadata fields to be added to xspf:track elements. xspf:track elements MAY contain zero or more meta elements.

<meta rel="http://example.org/key">value</meta>

### 4.1.1.2.14.1.1.1.12.1 attributes

### 4.1.1.2.14.1.1.1.12.1.1 rel

URI of a resource defining the metadata.

### 4.1.1.2.14.1.1.1.12.2 content

Value of the metadata element. This is character data, not HTML, and it may not contain markup. xspf:track elements MAY contain exactly one.

### 4.1.1.2.14.1.1.1.13 extension

The extension element allows non-XSPF XML to be included in XSPF documents. The purpose is to allow nested XML, which the meta and link elements do not. xspf:track elements MAY contain zero or more extension elements.

```xml
<playlist version="1" xmlns="http://xspf.org/ns/0/" xmlns:cl="http://example.com">
  <trackList>
    <track>
      <extension application="http://example.com">
        <cl:clip start="25000" end="34500"/>
      </extension>
    </track>
  </trackList>
</playlist>
```

### 4.1.1.2.14.1.1.1.13.1 attributes

### 4.1.1.2.14.1.1.1.13.1.1 application

URI of a resource defining the structure and purpose of the nested XML.

### 4.1.1.2.14.1.1.1.13.2 content

nested XML.

* * *

5 Requirements for XSPF generators
==================================

To ensure interoperability, conforming applications MUST generate playlists that follow the definitions listed in section 4 (element descriptions).

* * *

6 Requirements for XSPF players
===============================

6.1 Graceful failure
--------------------

If a media player is unable to render a track in the sequence, the player MUST NOT stop rendering the sequence and MUST attempt to continue at the next track. Players will frequently encounter resources that they cannot render — this is not a fatal error unless the player stops processing the playlist.

6.2 Relative paths
------------------

Relative paths MUST be resolved according to the [XML Base](http://www.w3.org/TR/xmlbase/) specification or [IETF RFC 2396](http://www.w3.org/TR/xmlbase/#RFC2396):

The rules for determining the base URI can be be summarized as follows (highest priority to lowest):
  The base URI is embedded in the document's content.
  The base URI is that of the encapsulating entity (message, document, or none).
  The base URI is the URI used to retrieve the entity.
  The base URI is defined by the context of the application.

Generators should take extra care to ensure that relative paths are correctly encoded. Do:

`<location>My%20Song.flac</location>`

Don't:

`<location>My Song.flac</location>`

6.3 Extension URIs
------------------

Applications MUST assign unique URIs from their own namespace for each link, meta and extension element. New URIs SHOULD be assigned if syntax and/or semantics of a link, meta or extension element changes.

* * *

References
==========

\[RFC2119\] [Bradner, S.](mailto:sob@harvard.edu "Harvard University"), "[Key words for use in RFCs to Indicate Requirement Levels](http://www.ietf.org/rfc/rfc2119.txt)", BCP14, RFC2119, March1997.

* * *

Author's Addresses
==================

Lucas Gonze  
E-mail: [lucas@gonze.com](mailto:lucas@gonze.com)  
Matthias Friedrich  
E-mail: [matt@mafr.de](mailto:matt@mafr.de)  
Robert Kaye  
E-mail: [rob@eorbit.net](mailto:rob@eorbit.net)  

* * *

Copyright and Patent Statements
===============================

Copyright © Xiph.org Foundation 2021.

The primary author of this work is [Lucas Gonze](http://gonze.com/about). He knows of no patent claims related to this work, and hereby forgoes any such claims with regard to his own contributions.

This work is licensed under a [Creative Commons Attribution-NoDerivs 2.5 License](http://creativecommons.org/licenses/by-nd/2.5/).
