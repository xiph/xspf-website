---
title: Validation
permalink: /validation
---

Validation
==========

If you are generating XSPF, your playlist should be valid according to one of the tools listed below. This is crucial for interoperability. Please take it seriously, despite the inconvenience.

1.  [Online XSPF validator](http://validator.xspf.org/) by Sebastian Pipping.
2.  [Relax NG (XML) Schema for XSPF-1](xspf-1_0.7.rng) by Matthias Friedrich. Sample usage of [XMLStarlet](http://xmlstar.sourceforge.net/):
    
    (Unix)     xmlstarlet validate -e -r xspf-1\_0.7.rng myplaylist.xspf
    (Windows)  xml.exe validate -e -r xspf-1\_0.7.rng myplaylist.xspf
    
    There also exists a schema for [XSPF-0](xspf-0_0.7.rng), but working with XSPF-1 should be preferred.
3.  [Relax NG (Compact) Schema for XSPF-1](xspf-1_0.7.rnc) by Matthias Friedrich. Sample usage of [RNV](http://sourceforge.net/projects/rnv/):
    
    (Unix)     rnv xspf-1\_0.7.rnc myplaylist.xspf
    (Windows)  rnv xspf-1\_0.7.rnc myplaylist.xspf
    
    There also exists a schema for [XSPF-0](xspf-0_0.7.rnc), but working with XSPF-1 should be preferred.
4.  [xspf\_check of libxspf](http://libspiff.sourceforge.net/) by Sebastian Pipping. Sample usage of xspf\_check:
    
    (Unix)     cat myplaylist.xspf | xspf\_check -
    (Windows)  xspf\_check - < myplaylist.xspf
    
5.  [XML Schema for XSPF](xspf-1_0.2.xsd) by Matthias Friedrich. Sample usage of [XMLStarlet](http://xmlstar.sourceforge.net/):
    
    (Unix)     xmlstarlet validate -e -s xspf-1\_0.2.xsd myplaylist.xspf
    (Windows)  xml.exe validate -e -s xspf-1\_0.2.xsd myplaylist.xspf
    

Note: The above XML Schema (XSD) is not fully spec compliant and therefore **deprecated**. Please use the Relax NG schema instead if you can. Since the XSD schema language is not powerful enough to express the XSPF specification there will never be a correct XSD schema for XSPF. Please see the mailing list archive for the discussion on this topic.
