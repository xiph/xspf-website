---
permalink: /quickstart
title: Quickstart
---

Quick Start Guide
=================

This document is a guide for people just encountering XSPF for the first time.

What is XSPF?
-------------

*   A playlist format like M3U
*   XML like RSS
*   Pronounced spiff
*   MIME type `application/xspf+xml`

What does XSPF look like?
-------------------------

A very simple document looks like this:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<playlist version="1" xmlns="http://xspf.org/ns/0/">
    <trackList>
        <track><location>file:///mp3s/song\_1.mp3</location></track>
        <track><location>file:///mp3s/song\_2.mp3</location></track>
        <track><location>file:///mp3s/song\_3.mp3</location></track>
    </trackList>
</playlist>
```

Notice that the file names are _URIs_, meaning that you could pass them to a web browser. Also notice that it's `track_L_ist`, with an uppercase L, not `track_l_ist`, with a lowercase l.

The following playlist is just the same except that the files are out on the web:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<playlist version="1" xmlns="http://xspf.org/ns/0/">
    <trackList>
        <track><location>http://example.com/song\_1.mp3</location></track>
        <track><location>http://example.com/song\_2.mp3</location></track>
        <track><location>http://example.com/song\_3.mp3</location></track>
    </trackList>
</playlist>
```

MIME type
---------

The MIME type for XSPF documents is `application/xspf+xml`. It is _NOT_ `text/plain`, `audio/xspf`, or `text/xml`. _THIS IS IMPORTANT_.

Metadata
--------

In the following section I'm going to show how to do standard metadata by giving example playlists for each item. I'll show each item by making a change in the following sample code:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<playlist version="1" xmlns="http://xspf.org/ns/0/">
    <trackList>
        <track>
            <location>http://example.com/song\_1.mp3</location>
        </track>
    </trackList>
</playlist>
```

How do I set metadata about the playlist, like the title, the name of the author, and the homepage of the author?

```xml
<?xml version="1.0" encoding="UTF-8"?>
<playlist version="1" xmlns="http://xspf.org/ns/0/">

    <!-- title of the playlist -->
    <title>80s Music</title>

    <!-- name of the author -->
    <creator>Jane Doe</creator>

    <!-- homepage of the author -->
    <info>http://example.com/~jane</info>

    <trackList>
        <track>
            <location>http://example.com/song\_1.mp3</location>
        </track>
    </trackList>
</playlist>
```

For a song in a playlist, how do I set metadata like the name of the artist and title of the album?

```xml
<?xml version="1.0" encoding="UTF-8"?>
<playlist version="1" xmlns="http://xspf.org/ns/0/">
    <trackList>
        <track>
            <location>http://example.com/song\_1.mp3</location>

            <!-- artist or band name -->
            <creator>Led Zeppelin</creator>

            <!-- album title -->
            <album>Houses of the Holy</album>

            <!-- name of the song -->
            <title>No Quarter</title>

            <!-- comment on the song -->
            <annotation>I love this song</annotation>

            <!-- song length, in milliseconds -->
            <duration>271066</duration>

            <!-- album art -->
            <image>http://images.amazon.com/images/P/B000002J0B.01.MZZZZZZZ.jpg</image>

            <!-- if this is a deep link, URL of the original web page -->
            <info>http://example.com</info>

        </track>
    </trackList>
</playlist>
```