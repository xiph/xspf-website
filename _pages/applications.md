---
permalink: /applications
title: Applications
---

*To add an application, report a broken link, or correct stale information, submit [an issue](https://gitlab.xiph.org/xiph/xspf-website/-/issues/).*

Programs
========

[AIMP](https://www.aimp.ru/)

> Powerful free audio player for Windows and Android that supports local files, NAS, clouds and podcasts. Additionally, it includes powerful tools to operate with audio files.

[Amarok](http://amarok.kde.org/) (Unix)

A feature-rich, library-oriented music player for Unix systems.

[Ampache](http://www.ampache.org)

According to the site, Ampache is a PHP-based tool for managing, updating and playing your MP3/OGG/RM/FLAC/WMA/M4A files via a web interface. It allows you to save playlists, create user accounts, and share your music catalogs with other Ampache servers.

[Armangil's podcatcher (Unix)](http://rubyforge.org/projects/podcatcher)

A podcast client for the command line which supports XSPF enclosures.

[Audacious](http://audacious-media-player.org/) (Unix)

A media player for Unix systems. Audacious is a fork of the Beep Media Player and therefore also descendant of XMMS.

[BMPx](http://beep-media-player.org/) (GNU/Linux)

A media player for GNU/Linux systems. A rewrite of [BMP](http://www.sosdg.org/%7Elarne/w/BMP_Homepage), a compact media player that was originally forked from XMMS (http://www.xmms.org) with the goal of porting XMMS to GTK2 and make use of more modern desktop standards.

[ccHost](http://wiki.creativecommons.org/Cchost) (web-based system supporting remixing and collaboration on media)

ccHost is an open source (GPL licensed) project that provides web-based infrastructure (PHP) to support collaboration, sharing, and storage of multi-media using the Creative Commons licenses and metadata. It is the codebase used by [ccMixter](http://ccmixter.org/) and other sites. Since version 3.0 cchost supports XSPF 1.0 (see [release notes 3.0](http://sourceforge.net/project/shownotes.php?release_id=445818&group_id=80503)).

[Cruxy Second-Life player](http://slurl.com/secondlife/Silver%20Shores/164/100/23/?img=https://www.cruxy.com/features/tbatb/images/sl/sltbatb1_001_sm.jpg%3Fv%3D0&msg=enter%20the%20listening%20lounge) (Second-Life XSPF player)

Cruxy Player for Second Life is a portable music player for use at listening parties, as a promotional giveaway, or for just some relaxing downtime in your personal parcel of land. It can load and play music from mp3 playlists using the XSPF standard. To provide maximum opportunity for creativity, the player is being released open-source, under the GNU General Public License.

[Drupal playlist module](http://drupal.org/handbook/modules/playlist) (blog/web publishing)

The playlist module allows \[publishers\] to arrange content into personal playlists. This module empowers users to become organizers of content as they re-order content from the site and create lists of their favorite content items. See also: [Audio and Playlist Modules for Drupal](http://drupal.org/node/32345) and [Drupal playlist demo page](http://playlist-demo.drupalart.org/).

[Easy Musicplayer For Flash](http://emff.sourceforge.net/)

Skinnable Flash MP3 player. See here for the [full list of features](http://emff.sourceforge.net/features/).

[FMP3 Flash XSPF Player](http://www.enricolai.com/fmp3/xspf-flash-mp3-player.asp)

A Flash-based application for playing music in web pages. Under [Creative Commons Attribution-NonCommercial-NoDerivs 2.5 license](http://creativecommons.org/licenses/by-nc-nd/2.5/), with additional conditions.

[foo\_xspf](http://maadman.ma.funpic.de/?page_id=4) (Windows, Foobar2000)

Adds XSPF read and write support to [Foobar2000](http://www.foobar2000.org/).

[Gnomoradio](http://gnomoradio.org/) (GNU/Linux)

A program that can find, fetch, share, and play music that is freely available for file sharing.

[Herrie](http://herrie.info/)

Herrie is a minimalistic music player that uses the command line. It is written to support a variety of operating systems, audio subsystems and file formats, including playlists.

[I/ON](http://openvision.tv/home/home.html) (Java, runs on MacOS X and Windows XP)

A media player oriented towards internet based content rather than locally stored content. Support for video formats like Quicktime, Windows Media, Flash, Ogg and MPEG formats; RSS/podcasts; BitTorrent. Makes available [Java source for XSPF support based on JDOM](https://ion.dev.java.net/source/browse/ion/IonXSPF/tv/openvision/xspf/).

[Jeroen Wijering's Flash MP3 Player](http://www.jeroenwijering.com/?item=Flash_MP3_Player)

A Flash-based application for playing music in web pages. Distinguished by an appealing look design, configurable behavior and appearance, and a Javascript/Actionscript API. Under [Attribution-NonCommercial-ShareAlike 2.0 license](http://creativecommons.org/licenses/by-nc-sa/2.0/).

[Joint Radio](http://www.jointradio.com)

A distributed playlist aggregator. It takes RSS bookmark feeds from a social bookmarking service (del.icio.us currently tested) and creates a playlist (XSPF, but that's not important here). Further, members can aggregate other user's playlists into a master playlist or use the whole site playlist.

[Jukes](http://melloware.com/products/jukes/index.html)

A cross platform MP3 player and organizer written in Java that uses XSPF as its playlist format.

[m3u2xspf](http://users.musicbrainz.org/%7Ematt/xspf/m3u2xspf)

A tool for converting M3U playlists to XSPF. This is a console tool written in Python, so for a Unix-like environment. It is free software under the Gnu GPL.

[Musicmobs / Mobster (OS X and Windows)](http://musicmobs.com/)

Playlist publication community and XSPF content resolver. Mobster creates iTunes playlists based on XSPF metadata by searching the local music collection for available tracks. The focus is on portability since the file location is ignored. Musicmobs acts as a playlist trading community by accepting XSPFs from Mobster or manual upload.

[Musicplayer](http://musicplayer.sourceforge.net/) (web)

A Flash-based application for playing music in web pages. Open source under the BSD license; can be used and modified by anyone, including for commercial purposes. Very popular both in its own right and as a source for derived applications.

[Ning](http://documentation.ning.com/post.php?Post:slug=XNCAlpha-FormatXSPF) (web development framework)

A hosted software development framework for web applications. The XSPF module allows applications to edit, parse, and publish XSPF documents.

[Paul Lamere's playlist resolver](http://contentresolver.com)

An experimental playlist resolver which will look at the track information and attempt to find information about the track at MusicBrainz, and fix or augment this information.

[PHP XSPF Playlist Generator](http://sourceforge.net/projects/php-xspf-gen/)

A PHP script that reads the contents of a folder containing MP3s, reads the ID3 tags and outputs an XSPF playlist. It means you just have to upload your files into a folder and then the playlist is generated automatically.

[Plait command line jukebox](http://plait.sourceforge.net/)

A script that creates playlists from a music library and submits the playlists to music players. Now includes an XSPF publishing feature which creates a complete web site including an XSPF playlist, a web page that embeds the XSPF Web Music Player, and MP3 files.

[Plext](http://plext.blogspot.com/) (Windows, Winamp)

Winamp plugin for advanced playlist functionality, including XSPF. Notable for supporting recursive playlists. (The author of this document uses this plugin often).

[Pod Pager](http://www.podpager.com/)

A screen scraper that accepts HTML as input and creates XSPF or RSS as output.

[QMMP](http://qmmp.ylsoftware.com/index_en.php)

Plugin-based audio player looking like XMMS or Winamp.

[Serpentine](http://s1x.homelinux.net/projects/serpentine/) (GNU/Linux)

Software for mastering audio CDs. It uses XSPF playlists to set the CD content.

[sift](http://code.google.com/p/sift/)

sift provides transformations that convert iTunes library files into other formats or extract useful information from them.

[Slim Server](https://github.com/Logitech/slimserver)

Controller for [Squeezebox](https://mysqueezebox.com/index/Home), a living room music device. Quote: You can punch an XSPF URL into the "Radio Tune In" field in our webUI. Or if you are browsing a Podcast or RSS feed that links to an XSPF document, that will work as well. The XSPF document does not need to be local.

[Spiffy](http://plurn.com/app/Download) (Windows)

A client application that downloads XSPF playlist content, effectively localizing the playlist. In an alpha state as of this writing (Jun 4, 2005), but potentially very useful when it grows up.

[Subsonic](http://subsonic.sourceforge.net/)

A media server that turns your browser into a jukebox. Free software under the LGPL.  
Quote: Subsonic is a free, web-based media streamer, providing access to your entire music collection wherever you are. Use it to share your music with friends, or to listen to your own music while at work. You can stream to multiple players simultaneously, for instance to one player in your kitchen and another in your living room.

[The Playlist Resolver](http://xspfresolver.com/) (web)

Takes an XSPF playlist and tries to enrich the tracks with more information from online sources. Output is a XSPF file again.

[Totem (Unix)](http://www.gnome.org/projects/totem/)

Official movie player of the GNOME desktop environment.

[Vanadium](http://sourceforge.net/projects/vanadium)

A Flash-based application for playing music (and videos) in web pages. Including JavaScript library to remote control the player. Open source under the LGPL license.

[VLC media player](http://www.videolan.org/)

A cross-platform media player. Under the GNU General Public License.

[WordPress XSPF Player Plugin](http://www.boriel.com/?page_id=12) (blog publishing; uses Musicplayer Flash app)

A popular tool for publishing weblogs. The XSPF plugin allows bloggers to edit and publish XSPF documents, then embed a Flash player in their weblog to render the XSPF.

[XMMS2](http://xmms2.sourceforge.net/)

Cross-platform multi-frontend successor to XMMS.

[XSPF Jukebox](http://blog.lacymorrow.com/projects/xspf-jukebox/)

Probably the best known of the many forks of [Musicplayer](#Musicplayer), enhanced with a number of features. See also the [online customization tool](http://blog.lacymorrow.com/projects/xspf-jukebox/xspf-customizer/) and the [embed generator](http://blog.lacymorrow.com/projects/xspf-jukebox/embed-generator/) for this player and the previous version which is now called [XSPF WMP](http://blog.lacymorrow.com/projects/xspf-wmp/).

[Yahoo! Music Jukebox](http://music.yahoo.com/musicengine/) (Windows)

The Windows-based client software for Yahoo! Music, which uses XSPF as its native playlist format. Said Yahoo's Ian Rogers: 

> We're trying to push forward an OPEN standard for playlisting, because it's ridiculous that it's 2005 and we're lacking even the basic currency for media exchange due to more than a decade of short-sighted proprietary verticals by media and software companies.

Websites
========

[360SoundCheck](http://www.austin360.com/music/content/music/soundcheck/form_upload.html)

360SoundCheck, an extension of the Austin360 music page, is an online music player dedicated to local musicians. This is a free service and any Austin band/artist is welcome to submit material using the online form.

[Amazon.com](http://amazon.com)

Online big box store uses XSPF for playing 30 second samples of MP3 files.

[ArtistServer](http://www.artistserver.com/)

A hosting service for musicians which makes playlists available in XSPF.

[BeepSNORT](http://beepsnort.com)

Turns any web page into an XSPF file and runs it through a Flash player.

[Digital Archive Streamer](http://waxandwane.org/das/)

An alternative streaming front-end to digital archives of field and 78 rpm recordings. The original archive sites have searching front-ends, but users have to click on each tune individually to play it. Using the DAS site users can create a custom playlist of tunes and stream the entire list. Uses AJAX to display selections as the user types a [Perl regular expression](http://www.perl.com/doc/manual/html/pod/perlre.html) for filtering. The back-end is 100% XSPF using [XML::XSPF](#cpan) and the user chooses between XSPF, M3U, PLS, or RAM playlist formats for streaming.

[earfl](http://earfl.com/)

Website allowing you to record and host audio stories and slideshows, and make them available in XSPF.

[feedpixel.com](http://feedpixel.com)

a free online feed reader and podcast player service.

[Foafing the music](http://foafing-the-music.iua.upf.edu/index.html)

A music recommendation system, based on user's profile. Uses the [FOAF](http://www.foaf-project.org/) vocabulary to recommend music. Looks promising, but seems to be in an experimental phase at this moment.

[Grabb.it](http://grabb.it/)

An online music player that will create playlists for any web page that links to music files. You can remember, download, and share the songs you find.

[iLike](http://www.ilike.com/)

A music site oriented towards social-networks; for sharing music tastes and playlists, and learning about new artists and concerts together.

[Internet Archive Netlabels](http://www.archive.org/details/netlabels)

... hosts complete, freely downloadable/streamable, often Creative Commons-licensed catalogs of 'virtual record labels'.

[Jamendo](http://www.jamendo.com/us/static/concept/)

A repository of artists and music that allows users to create XSPF playlists.

[Jinzora](http://jinzora.com/) (web)

A web based media streaming and management system, designed to stream audio and video files to any internet connected computer.

[Last.fm / Audioscrobbler](http://www.last.fm/)

A listener community grown out of Audioscrobbler. Listeners send information about each track they play to Last.fm. That way Last.fm can build statistics about your music habits and make suggestions for music you might want to check out in the future.

[Magnatune](http://www.magnatune.com/)

A "non-evil" record label offering free downloads of all their albums. You can _try before you buy_: Listen to 128KB/s MP3s for free or buy a higher quality (or even lossless) version if you like it. 50% go directly to the artist. More details [here](http://www.magnatune.com/info/whynotevil).

[Myplaylist](http://www.myplaylist.biz/)

combines relevant images into a music slideshow playlist. [Myplaylist lyrics](http://www.myplaylist.biz/lyrics/) combines music slideshows with accompanying lyrics. These mashups combine content (Flickr / Seeqpod) which is then reformulated and made available via the Myplaylist [API](http://www.myplaylist.biz/api/).

[MyStrands](http://www.mystrands.com/)

A music-oriented social site focused on taste-sharing and recommendations

[playlistar.com](http://www.playlistar.com/)

Browser-based playlist manager for audio files you find while wandering through the halls of music - crosslink tracks you find into your account and listen to them at your leisure from anywhere.

[Playr](http://playr.hubmed.org/) (web)

A web-based tool for screen-scraping web pages and emitting the results as a playlist. Says Playr creator Alf Eaton: There's a Flash/XSPF bookmarklet on the front page -- which you can use to directly play any page containing linked MP3s -- as well as 'Flash' links on the New Playlists page.

[Plurn](http://plurn.com/app/About/) (web)

A web-based playlist community modeled after Webjay.

Project Opus (defunct)

A music community site which makes playlists available in XSPF.

[Seeqpod Playabale Search](http://www.seeqpod.com/)

Audio and video search engine with online playlists. XSPF is used in the [SeeqPod REST API](http://www.seeqpod.com/api/).

[ultraPh0nZ FMP256](http://www.draftlight.net/dnex/mp3player/free/) (web)

A Flash application for playing music in web pages. Skinnable and slick.

[Webjay](http://webjay.org/) (web)

A playlist community site and playlist authoring tool which exports XSPF playlists.

Tools
=====

[fspiff](http://antiklimax.se/projects/fspiff/)

Command line tool to produce an XSPF playlist from M3U or a list of filenames.

[xspf-download-0.1.0.py](http://mayhem-chaos.net/stuff/xspf-download-0.1.0.py) (GNU/Linux)

A Python script that parses XSPF and M3U files and downloads the files to the local harddrive.

[XSPF.PHP](http://xspfphp.sourceforge.net/)

Generates XSPF playlists from MP3 tags.

[XSPF playlist generator](http://emff.sourceforge.net/playlistgenerator/)

Live playlist editor/generator implemented in JavaScript

[XSPF to JSPF Parser](http://github.com/jchris/xspf-to-jspf-parser) (Javascript)

Converts your XSPF files to [JSPF](http://wiki.xiph.org/index.php/JSPF_Draft) ("XSPF on JSON") online.

[XSPF Validator](http://validator.xspf.org/)

Online validator for XSPF documents.

Libraries
=========

[cl-xspf](http://www.cliki.net/cl-xspf) (Common Lisp)

A Common Lisp API for writing XSPF playlists.

[haXe XSPF](http://www.xspf.disktree.net/) ([haXe](http://www.haxe.org/intro))

XSPF support for haXe web applications.

[JSpiff](http://melloware.com/products/jspiff/) (Java)

JSpiff is a Java API for reading and writing XSPF ("Spiff") open XML playlists using JAXP or JAXB.

[libxspf](http://libspiff.sourceforge.net/) (C, C++)

The official reference implementation of XSPF by the Xiph.Org Foundation. libxspf (formerly called libSpiff) is a cross-platform C++ library for reading and writing XSPF playlist files (both Version 0 and 1). It is licensed under the New BSD license. See also [libxspf tutorial](http://libspiff.sourceforge.net/doc/html/ "XSPF C++ library tutorial").

[Lizzy](http://sourceforge.net/projects/lizzy/) (Java)

Java library and tools allowing to parse, create, edit, convert and save almost any kind of multimedia playlist. Amongst other formats, XSPF is of course supported.

[PEAR :: Package :: File\_XSPF](http://pear.php.net/package/File_XSPF) (PHP)

PHP library for parsing and generating XSPF.

[Ruby XSPF playlist class](http://rubyforge.org/projects/xspf) (Ruby)

A Ruby library to parse XML Shareable Playlist Format (XSPF) documents and export them to M3U, SMIL, HTML and SoundBlox. License: GNU General Public License (GPL) version 2, Ruby License.

[XML::XSPF on CPAN](http://search.cpan.org/%7Edaniel/XML-XSPF/lib/XML/XSPF.pm) (Perl)

Perl library for parsing and generating XSPF.

[XSPF.toJSPF()](http://github.com/jchris/xspf-to-jspf-parser) (JavaScript)

XSPF.toJSPF() is a function for parsing XSPF in Javascript. The resulting Javascript object is easy to use and manipulate to create client-side playlist browsers and media players. The parsed result can also easily be serialized into JSON for easy server-client transport. This allows developers to use one player for handling both XML XSPF and JSON JSPF playlists provided by the server.

[XSPF4PHP](http://php4xspf.berlios.de/) (PHP)

Set of classes for creating XSPF files from PHP.

---

