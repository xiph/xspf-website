---
title: Home
---

# XSPF Home

XSPF is the XML format for sharing playlists.

- It is *portable*. You should be able to carry a playlist from one place to another.
- It is *well-engineered*. Most playlist formats have bugs that make life harder for programmers and users.
- It is *free* (as in liberty) and *open*. No proprietary lock-in.

# Need JSON instead of XML? 

Use [JSPF](/jspf), the JSON Shareable Playlist Format.